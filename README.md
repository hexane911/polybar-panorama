# polybar-panorama

![](https://codeberg.org/hexane911/polybar-panorama/raw/commit/a8b4c18b4e25152fab820121417a2633ee6f58d2/demo.gif)

###### Latest IA Panorama new running line


## Dependencies

NodeJS (tested on 17, recomended to install using nvm)
Yarn

Run this snippet fro quick installation

```Bash
wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash && export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" && nvm i node && npm i -g yarn 
```

## Configuration

Add code from `example.ini` to your polybar config, set needed path 

Go to `polybar-panorama` directory and run `yarn`

In `index.js` you can set your values in the begining of the file

|Name|Type|Purpose|
|----|----|-------|
|`msgLenght`|`number`|Lenght of the running line without separators|
|`updateTimeSeconds`|`number`|Amount of seconds between news change|
|`prefix`, `suffix`|`string`|Text before and after the running line|
|`separator`|`string`|Text between iterations of message|
|`speed`|`number`|Running line update speed in seconds|
|`path_and_name`|`string`|Path to save `current_link` file, used to allow opening it in browser|

