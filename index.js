#!/usr/bin/env node
const cheerio = require('cheerio')
const axios = require('axios')
const fs = require("fs");
const os = require('os')

const source = "https://panorama.pub";





//Config section
const msgLenght = 50;
const updateTimeSeconds = 120;
const prefix = "<a> ";
const suffix = " </a>";
const separator = "        ";
const speed = 0.2;
const path_and_name = os.homedir() + '/.config/polybar/scripts/polybar-panorama/current_link'
////////////////////////////////////////////////////////////////////////////












let iteration = 0;
const getTitle = async () => {
  const page = await axios.get(source).then((res) => res.data);
  const getData = (page) => {
    const data = [];
    const $ = cheerio.load(page);
    $("a.entry").each((i, el) => {
      data.push({
        title: $(el).find("h3").text(),
        link: `${source}${$(el).attr("href")}`,
      });
    });
    return data;
  };
  const titles = getData(page);
  const title = titles[Math.floor(Math.random() * titles.length)];
  const stream = fs.createWriteStream(path_and_name)
  stream.on('open', async ()=> {
      await stream.write(title.link)
  })
  console.log(title)
  return { title: title.title, link: title.link };
};

let char = 0;

const draw = ({ title, link }) => {
  // console.clear();
  const raw = separator + title.trim();
  const cursor = char % raw.length;
  const start = raw.slice(cursor, cursor + msgLenght);
  const end = raw.slice(0, msgLenght - start.length);
  const formatted = start + end;
  console.log(prefix + formatted + suffix);
  char++;
};

let msg = "";
let link = "";

const app = setInterval(async () => {
  if (msg == "" || link == "") {
    const response = await getTitle();
    msg = response.title;
    link = response.link;
  } else {
    if (iteration >= updateTimeSeconds) {
      msg = "";
      link = "";
      iteration = 0;
      char = 0;
    } else {
      draw({ title: msg, link: link });
      iteration += speed;
    }
  }
}, speed * 1000);
